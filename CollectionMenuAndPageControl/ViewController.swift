//
//  ViewController.swift
//  CollectionMenuAndPageControl
//
//  Created by Khanathip Rachprakhon  on 6/3/2567 BE.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControlView: PageControlView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initCollectionView()
        
        let collectionViewSize = collectionView.frame.size

        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let contentSize = flowLayout.collectionViewContentSize
            pageControlView.apply(displaySize: collectionViewSize, contentSize: contentSize)
        }
    }


}
extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func initCollectionView() {
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        self.collectionView.showsHorizontalScrollIndicator = false
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.iconImageView.image = UIImage(named: "vector")
        cell.label.text = "menu"
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let cellsAcross: CGFloat = 5
        let width = (self.view.bounds.width / cellsAcross)
        let height = (collectionView.bounds.height / 2)

        return CGSize(width: width, height: height)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControlView.updateView(contentOffset: scrollView.contentOffset)
    }
}
