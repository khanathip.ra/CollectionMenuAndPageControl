//
//  PageControlView.swift
//  CollectionMenuAndPageControl
//
//  Created by Khanathip Rachprakhon  on 6/3/2567 BE.
//

import Foundation
import UIKit

class PageControlView: UIView {

    let view = UIView()
    var width = 0.0
    
    var displaySize = CGSize()
    var contentSize = CGSize()
    
    var selectColor: UIColor = .red
    var didselectColor: UIColor = .black
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
      }
    
    func setupView(){
        self.backgroundColor = .black
        self.layer.cornerRadius = self.bounds.width / 12
        self.layer.masksToBounds = true

        self.backgroundColor = didselectColor
        view.backgroundColor = selectColor
        
        self.addSubview(view)
        self.layoutIfNeeded()
        
    }
    func apply(displaySize: CGSize, contentSize: CGSize) {
        self.displaySize = displaySize
        self.contentSize = contentSize
        
        width = self.bounds.width * (((displaySize.width * 100) / contentSize.width) / 100)

        view.frame = CGRect(x: 0, y: 0, width: width, height: self.bounds.height)
        
    }
    func updateView(contentOffset: CGPoint){
        
        let x = self.bounds.width * (((contentOffset.x * 100) / contentSize.width) / 100)
        
        view.frame = CGRect(x: x, y: 0, width: width, height: self.bounds.height)
    }
}
