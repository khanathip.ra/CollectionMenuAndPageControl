//
//  CollectionViewCell.swift
//  CollectionMenuAndPageControl
//
//  Created by Khanathip Rachprakhon  on 6/3/2567 BE.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
